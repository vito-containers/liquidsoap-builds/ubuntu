FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c AS downloader

# renovate: datasource=github-tags depName=savonet/liquidsoap-release-assets
ARG LIQUIDSOAP_VERSION=2.3.1

ARG LIQUIDSOAP_RELEASE="https://github.com/savonet/liquidsoap-release-assets/releases/download/v${LIQUIDSOAP_VERSION}"
ARG LIQUIDSOAP_DEB="liquidsoap-minimal_${LIQUIDSOAP_VERSION}-ubuntu-noble-1_amd64.deb"
ARG LIQUIDSOAP_SHA512SUM="ed7ea107a0497cb5c423ecbb2d392c50d8ad1f4979dc30fd0b5e3f774119ef750ab353f6df6a4775df17d2e2d3b8d624e0086a476b02c28caad1cea606926b9f"

# hadolint ignore=DL3047,DL4006
RUN set -eux; \
      mkdir /packages; \
      wget "${LIQUIDSOAP_RELEASE}/${LIQUIDSOAP_DEB}" -O /packages/liquidsoap.deb; \
      echo "${LIQUIDSOAP_SHA512SUM}  /packages/liquidsoap.deb" | sha512sum -c -;

FROM registry.gitlab.com/vito-containers/ubuntu:24.04@sha256:9c89565a5498da619980804aeb2511b363055b953cf6ad3cf456eeb991eef0c9

ENV TZ=UTC

# renovate: datasource=repology depName=ubuntu_24_04/ca-certificates versioning=loose
ARG CA_CERTIFICATES_VERSION=20240203
# renovate: datasource=repology depName=ubuntu_24_04/gosu versioning=loose
ARG GOSU_VERSION=1.17-1ubuntu0.24.04.2
# renovate: datasource=repology depName=ubuntu_24_04/tzdata versioning=loose
ARG TZDATA_VERSION=2024b-0ubuntu0.24.04.1

# hadolint ignore=DL3009
RUN --mount=type=cache,target=/var/cache/apt-v,sharing=locked,id=ubuntu_24.04_apt_cache \
    --mount=type=bind,target=/packages,source=/packages,from=downloader \
    set -eux; \
      apt-get-v update; \
      apt-get-v install \
        ca-certificates="${CA_CERTIFICATES_VERSION}" \
        gosu="${GOSU_VERSION}" \
        tzdata="${TZDATA_VERSION}" \
        /packages/liquidsoap.deb \
      ; \
      apt-get-v clean-old;

COPY rootfs /

ENTRYPOINT [ "entrypoint" ]
CMD [ "liquidsoap", "/var/liquidsoap/default.liq" ]
